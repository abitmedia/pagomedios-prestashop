<link rel="stylesheet" href="{$module_dir|escape:'htmlall':'UTF-8'}views/css/waitMe.min.css" type="text/css" media="all">

<div class="row">
    <div class="row">
        <div class="col-12">
            <div id="pagomedios-errors"></div>
        </div>

        <div class="col-md-3 col-xs-1"></div>
        <div class="col-md-5 col-xs-8">
            <form
					method="post"
{*					action="{$link->getModuleLink('pagomedios', 'redirect', [], true)|escape:'html':'UTF-8'}"*}
					action="#"
					id="form-payment"
                    name="pagomedios-form-payment"
					class="product-variants">

                <div class="form-group product-variants-item">
                    <span class="control-label">Tipo de empresa</span>
                    <select name="billing_company_type" class="form-control form-control-select">
                        <option value="Persona Natural">Persona Natural</option>
                        <option value="Empresa">Empresa</option>
                    </select>
                </div>
                <div class="form-group product-variants-item">
                    <span class="control-label">Tipo de documento</span>
                    <select name="billing_document_type" class="form-control form-control-select">
                        <option value="01">Cédula de identidad</option>
                        <option value="02">RUC</option>
                        <option value="03">Pasaporte</option>
                        <option value="04">ID del exterior</option>
                    </select>
                </div>
                <div class="form-group product-variants-item">
                    <span class="control-label">Identificación</span>
                    <input type="text" name="billing_document" class="form-control" value=""
                           style="background-color: #fff !important;">
                </div>
            </form>
        </div>
        <div class="col-md-4 col-xs-9"></div>
    </div>
    <br/>
</div>

<script type="text/javascript" defer src="{$module_dir|escape:'htmlall':'UTF-8'}views/js/waitMe.min.js"></script>
<script>
    $(function() {
        $('[name="pagomedios-form-payment"]').submit(function(e) {
            e.preventDefault();
            const url = "{$link->getModuleLink('pagomedios', 'chargeajax', [], true)|escape:'html':'UTF-8'}";
            const request = {
                billing_company_type: $('[name="billing_company_type"]').val(),
                billing_document_type: $('[name="billing_document_type"]').val(),
                billing_document: $('[name="billing_document"]').val(),
            };

            hideErrors();
            waitMe('Espere un momento');

            $.ajax({
                url: url,
                data: request,
                type: 'POST',
                dataType: 'json',
                success: function (data) {
                    if (!data.status) {
                        showErrors(data.errors);
                        return false;
                    }

                    redirect(data.url);
                }
            }).always(function() {
                $('body').waitMe('hide');
                $('#payment-confirmation > .ps-shown-by-js > button').prop('disabled', false);
            });

            return false;
        });
    });

    function redirect(url) {
        setTimeout(function() {
            waitMe('Redirigiéndose a Pagomedios, no cierre esta ventana.');
        }, 100);

        setTimeout(function() {
            window.location = url;
        }, 500);
    }

    function waitMe(message) {
        $('body').waitMe({
            effect: 'orbit',
            text: message,
            bg: 'rgba(255,255,255,0.7)',
            color:'#28d2c8'
        });
    }

    function showErrors(errors) {
        const $el = $('#pagomedios-errors');

        $el.removeClass('hidden');
        $el.addClass('alert');
        $el.addClass('alert-danger');
        $el.html('<strong>Error ' + errors + '</strong>');
    }

    function hideErrors() {
        const $el = $('#pagomedios-errors');
        $el.addClass('hidden');
    }
</script>