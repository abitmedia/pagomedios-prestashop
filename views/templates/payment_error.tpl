{extends file='page.tpl'}
{block name='content'}
    <div>
        <ul class="alert alert-danger">
            <li>Tu pago no fue autorizado, por favor inténtalo nuevamente.</li>
        </ul>
    </div>

    <script type="text/javascript">
        function Redirect() {
            window.location = "{$urls.base_url}";
        }
        setTimeout('Redirect()', 60000);
    </script>
{/block}
