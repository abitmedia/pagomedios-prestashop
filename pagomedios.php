<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

class Pagomedios extends PaymentModule
{
    const URL = 'https://cloud.abitmedia.com/api/payments/create-payment-request';

    private $_postErrors = [];

    public $limited_countries;
    public $limited_currencies;
    public $limited_languages;

    public function __construct()
    {
        $this->name = 'pagomedios';
        $this->tab = 'payments_gateways';
        $this->version = '1.0.0';
        $this->author = 'Abitmedia Cloud';
        $this->bootstrap = true;
        $this->display = 'view';
//        $this->controllers = ['chargeajax','postpayment'];

        parent::__construct();

        $this->displayName = $this->l('Pagomedios');
        $this->description = $this->l('Cobra fácil, rápido y seguro por Internet!');
        $this->meta_title = 'Pagomedios';
        $this->confirmUninstall = $this->l('When uninstalling, you will not be able to receive payments.Are you sure?');

        $this->limited_countries = ['PE', 'EC', 'CO'];
        $this->limited_currencies = ['PEN', 'USD', 'COP'];
        $this->limited_languages = ['ES', 'EN'];

        $this->ps_versions_compliancy = ['min' => '1.6', 'max' => _PS_VERSION_];
    }

    public function install()
    {
        if (extension_loaded('curl') == false)
        {
            $this->_errors[] = $this->l('You have to enable the cURL extension on your server to install this module');
            return false;
        }

        $iso_code = Country::getIsoById(Configuration::get('PS_COUNTRY_DEFAULT'));

        /*if (in_array($iso_code, $this->limited_countries) == false)
        {
            $this->_errors[] = $this->l('This module is not available in your country');
            return false;
        }*/

        Configuration::updateValue('PAGOMEDIOS_TITLE', $this->l('Continue with Pagomedios'));
        Configuration::updateValue('PAGOMEDIOS_LIVE_MODE', false);
        Configuration::updateValue('PAGOMEDIOS_NOTIFICATION_URL', $this->context->link->getModuleLink('pagomedios', 'notification'));

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('payment') &&
            $this->registerHook('paymentReturn') &&
            $this->registerHook('paymentOptions');
    }

    public function uninstall()
    {
        Configuration::deleteByName('PAGOMEDIOS_TITLE');
        Configuration::deleteByName('PAGOMEDIOS_LIVE_MODE');
        Configuration::deleteByName('PAGOMEDIOS_NOTIFICATION_URL');
        /*Configuration::deleteByName('SAFETYPAY_API_KEY');
        Configuration::deleteByName('SAFETYPAY_SIGNATURE_KEY');
        Configuration::deleteByName('SAFETYPAY_EXPIRATION_TIME');
        Configuration::deleteByName('SAFETYPAY_ENVIRONMENT');
        Configuration::deleteByName('SAFETYPAY_ORDER_STATUS');
        Configuration::deleteByName('SAFETYPAY_TITLE');
        Configuration::deleteByName('SAFETYPAY_INFO_MESSAGE');
        Configuration::deleteByName('SAFETYPAY_CALLBACK');
        Configuration::deleteByName('VEX_LICENSE');
        Configuration::deleteByName('VEX_ACTIVATED');*/

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        $this->_html = '';

        if (Tools::isSubmit('btnSubmit'))
        {
            $this->_postValidation();

            if (!count($this->_postErrors))
            {
                $this->postProcess();
            } else {
                foreach ($this->_postErrors as $error)
                {
                    $this->_html .= $this->displayError($error);
                }
            }
        }

        $this->_html .= $this->_displayInfo();
        $this->_html .= $this->renderForm();

        return $this->_html;


        /*$this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');
        if (((bool)Tools::isSubmit('submitSafetypayModule')) == true) {
            if ($this->postProcess()) {
                $m = $this->displayConfirmation($this->l('Your data was saved successfully')) . $output;
                return $m . $this->renderForm();
            } else {
                $m = $output . $this->displayError($this->l('At least one of the fields is incorrect, please check'));
                return $m . $this->renderForm();
            }
        }


        return $output . $this->renderForm();*/
    }

    /**
     * @return bool|string
     */
    private function _displayInfo()
    {
        return false;
    }

    /**
     * Admin Zone
     * Create the form that will be displayed in the configuration of your module.
     *
     * @return string
     * @throws PrestaShopException
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = [
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        ];

        return $helper->generateForm([$this->getConfigForm()]);
    }

    private function getOrderState()
    {
        $os = [];

        foreach (OrderStateCore::getOrderStates($this->context->language->id) as $item) {
            $os[] = [
                'id_feature' => $item['id_order_state'],
                'name' => $item['name'],
            ];
        }
        return $os;
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return [
            'form' => [
                'legend' => [
                    'title' => $this->l('Settings'),
                ],
                'input' => [
                    [
                        'type' => 'text',
                        'label' => $this->l('Título en el Checkout'),
                        'name' => 'PAGOMEDIOS_TITLE',
                        'size' => 150,
                        'required' => false
                    ],
                    [
                        'type' => 'text',
                        'label' => $this->l('Token de Comercio'),
                        'name' => 'PAGOMEDIOS_COMMERCE_TOKEN',
                        'desc' => 'Encuentre su token en: Abitmedia Cloud -> Panel de usuario -> Mi empresa -> Token',
                        'required' => true,
                        'disabled' => false
                    ],
                    [
                        'type' => 'radio',
                        'label' => $this->l('Pasarela de Pagos'),
                        'name' => 'PAGOMEDIOS_PAYMENT_GATEWAY',
                        'desc' => 'Asegurese de seleccionar la pasarela que tenga configurada en Pagomedios, caso contrario se procesará con la que tiene contratada. ¿No tiene configurada ningún pasarela? <a href="https://abitmedia.com/contactos/" target="_blank">Contáctese con soporte</a>',
                        'required' => true,
                        'class' => 't',
//                        'is_bool' => true,
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Alignet Payme')
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 2,
                                'label' => $this->l('Payphone')
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 3,
                                'label' => $this->l('Datafast')
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 4,
                                'label' => $this->l('Paymentez')
                            ],
                        ],
                    ],
                    [
                        'type' => 'radio',
                        'label' => $this->l('Facturación Electrónica'),
                        'name' => 'PAGOMEDIOS_ELECTRONIC_BILLING',
                        'desc' => '¿No tiene contratado este módulo? <a href="https://abitmedia.com/contactos/" target="_blank">Contáctese con ventas</a>',
                        'required' => true,
                        'class' => 't',
                        'values' => [
                            [
                                'id' => 'active_on',
                                'value' => 0,
                                'label' => $this->l('NO')
                            ],
                            [
                                'id' => 'active_off',
                                'value' => 1,
                                'label' => $this->l('SI')
                            ],
                        ],
                    ],
                    [
                        'type' => 'select',
                        'label' => $this->l('Order status after confirmation'),
                        'name' => 'PAGOMEDIOS_ORDER_STATUS',
                        'options' => [
                            'query' => $this->getOrderState(),
                            'id' => 'id_feature',
                            'name' => 'name'
                        ],
                        'required' => true
                    ],

                    [
                        'type' => 'label',
                        'label' => '',
                        'desc' => $this->l('Pagomedios - https://pagomedios.com/'),
                        'name' => 'label'
                    ]
                ],
                'submit' => [
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default pull-right'
                ]
            ]
        ];
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return [
            /*'VEX_LICENSE' => Configuration::get('VEX_LICENSE'),
            'SAFETYPAY_API_KEY' => Configuration::get('SAFETYPAY_API_KEY'),
            'SAFETYPAY_SIGNATURE_KEY' => Configuration::get('SAFETYPAY_SIGNATURE_KEY'),
            'SAFETYPAY_EXPIRATION_TIME' => Configuration::get('SAFETYPAY_EXPIRATION_TIME'),
            'SAFETYPAY_ENVIRONMENT' => Configuration::get('SAFETYPAY_ENVIRONMENT'),
            'SAFETYPAY_ORDER_STATUS' => Configuration::get('SAFETYPAY_ORDER_STATUS'),
            'SAFETYPAY_TITLE' => Configuration::get('SAFETYPAY_TITLE'),
            'SAFETYPAY_INFO_MESSAGE' => Configuration::get('SAFETYPAY_INFO_MESSAGE'),
            'SAFETYPAY_CALLBACK' => Configuration::get('SAFETYPAY_CALLBACK'),*/

            'PAGOMEDIOS_TITLE' => Configuration::get('PAGOMEDIOS_TITLE'),
            'PAGOMEDIOS_COMMERCE_TOKEN' => Configuration::get('PAGOMEDIOS_COMMERCE_TOKEN'),
            'PAGOMEDIOS_PAYMENT_GATEWAY' => Configuration::get('PAGOMEDIOS_PAYMENT_GATEWAY'),
            'PAGOMEDIOS_ELECTRONIC_BILLING' => Configuration::get('PAGOMEDIOS_ELECTRONIC_BILLING'),
            'PAGOMEDIOS_ORDER_STATUS' => Configuration::get('PAGOMEDIOS_ORDER_STATUS'),
        ];
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key)
        {
            Configuration::updateValue($key, Tools::getValue($key), true);
        }

        return true;
    }

    /**
     * Show payment options in version 1.6
     *
     * @param $params
     * @return array|string|mixed
     */
    public function hookPayment($params)
    {
        if (!$this->canBeUsed($params))
        {
            return;
        }

        $this->smarty->assign(['module_dir', $this->_path, 'payment_message' => Configuration::get('SAFETYPAY_INFO_MESSAGE'), 'title' => Configuration::get('SAFETYPAY_TITLE')]);
        return $this->display(__FILE__, 'views/templates/hook/ps16/payment.tpl');
    }

    /**
     * Show payment options in version 1.7
     *
     * Reference:
     * https://github.com/prestalab/universalpay/blob/master/views/templates/front/payment_execution.tpl
     *
     * @param $params
     * @return array|string|mixed
     * @throws SmartyException
     */
    public function hookPaymentOptions($params)
    {
        if (!$this->canBeUsed($params))
        {
            return null;
        }

        $option = new PrestaShop\PrestaShop\Core\Payment\PaymentOption();
        $option->setModuleName($this->name)
            ->setCallToActionText($this->getCheckoutTitle())
            ->setAction($this->context->link->getModuleLink($this->name, 'redirect', [], true))
            ->setLogo(Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/views/img/logo_cards.jpg'));

        $option->setForm(
            $this->context->smarty->fetch('module:pagomedios/views/templates/payment.tpl')
        );

        return [$option];
    }

    public function hookDisplayTop()
    {
        $controller = $this->context->controller;

        if ($controller->php_self != 'order' && $controller->php_self != 'order-opc') {
            return false;
        }
        /*
            You can do custom logic here if you want to display message only
            on some conditions or only on specific step of the checkout
        */
        $controller->errors[] = $this->l('Some message');

        return false;
    }


    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    /**
     * This hook is used to display the order confirmation page.
     */
    public function hookPaymentReturn($params)
    {
        if (!$this->canBeUsed($params))
        {
            return;
        }

        $order = $params['order'];

        if ($order->getCurrentOrderState()->id != Configuration::get('PS_OS_ERROR'))
        {
            $this->smarty->assign('status', 'ok');
        }

        $currency = new Currency($params['cart']->id_currency);
        $this->smarty->assign(array(
            'id_order' => $order->id,
            'reference' => $order->reference,
            'params' => $params,
            'total' => Tools::displayPrice($order->total_paid, $currency, false),
            'shop_name' => Configuration::get('PS_SHOP_NAME')
        ));

        return $this->display(__FILE__, 'views/templates/hook/confirmation.tpl');
    }

    /*
    * Function for generate the order state
    */
    public function installOrderState()
    {
        if (Configuration::get('PS_OS_SAFETYPAY_PENDING_PAYMENT') < 1) {
            $order_state = new OrderState();
            $order_state->send_email = false;
            $order_state->module_name = $this->name;
            $order_state->invoice = false;
            $order_state->color = '#98c3ff';
            $order_state->logable = true;
            $order_state->shipped = false;
            $order_state->unremovable = false;
            $order_state->delivery = false;
            $order_state->hidden = false;
            $order_state->paid = false;
            $order_state->deleted = false;
            $lang = (int)Configuration::get('PS_LANG_DEFAULT');
            $order_state->name = array($lang => pSQL($this->l('SafetyPay - Pending payment')));
            if ($order_state->add()) {
                // We save the order State ID in Configuration database
                Configuration::updateValue('PS_OS_SAFETYPAY_PENDING_PAYMENT', $order_state->id);
            } else {
                return false;
            }
        }
        return true;
    }

    private function canBeUsed($params)
    {
        if (!$this->active)
        {
            return false;
        }

        $currency = new Currency($params['cart']->id_currency);
        $currency->iso_code;

        if (in_array($currency->iso_code, $this->limited_currencies) == false)
        {
            return false;
        }

        if (empty(Configuration::get('PS_LANG_DEFAULT'))
            || empty(Configuration::get('PAGOMEDIOS_COMMERCE_TOKEN'))
            || empty(Configuration::get('PAGOMEDIOS_PAYMENT_GATEWAY'))
        ) {
            return false;
        }

        return true;
    }

    private function _postValidation()
    {
        if (!Tools::getValue('PAGOMEDIOS_COMMERCE_TOKEN'))
        {
            $this->_postErrors[] = $this->l('The Commerce Token field is required.');
        }
        if (!Tools::getValue('PAGOMEDIOS_PAYMENT_GATEWAY'))
        {
            $this->_postErrors[] = $this->l('The Payment Gateway field is required.');
        }
    }

    /**
     * @return string
     */
    private function getCheckoutTitle()
    {
        return empty(Configuration::get('PAGOMEDIOS_TITLE'))
            ? $this->l('Continue with Pagomedios')
            : Configuration::get('PAGOMEDIOS_TITLE');
    }

    public function makeRequest($companyType, $documentType, $document)
    {
        $customer = $this->context->customer;
        $cart = $this->context->cart;
        $amount = round($cart->getOrderTotal(true), 2);

        $address = new Address((int) $cart->id_address_invoice);

        $notificationUrl = $this->context->link->getModuleLink('pagomedios', 'notification', [], true);

        $headers = [
            "Content-Type: application/x-www-form-urlencoded",
            "Postman-Token: 3724770d-a4c7-4330-97dc-6d66237dbc19",
            "cache-control: no-cache",
            sprintf('Authorization: Bearer %s', Configuration::get('PAGOMEDIOS_COMMERCE_TOKEN'))
        ];

        $withoutIva = round($amount / 1.12, 2);
        $iva = round($amount - $withoutIva, 2);

        $data = [
            'companyType' => $companyType,
            'document' => $document,
            'documentType' => $documentType,
            'fullName' => $customer->firstname . ' ' . $customer->lastname,
            'address' => $this->getAddress($address),
            'mobile' => $this->getPhone($address),
            'email' => $customer->email,
            'description' => 'Orden de compra #' . $cart->id,

            'amount' => $amount, //Total con IVA
            'amountWithTax' => $withoutIva, //Subtotal con impuestos IVA 12%
            'amountWithoutTax' => 0,  //Subtotal sin impuestos IVA 0%
            'tax' => $iva, //Total impuestos, Total IVA

            'gateway' => Configuration::get('PAGOMEDIOS_PAYMENT_GATEWAY'),
            'notifyUrl' => $notificationUrl, //Se enviara un array por POST, detalle archivo respuesta.php
            'reference' => $cart->id,
            'generateInvoice' => Configuration::get('PAGOMEDIOS_ELECTRONIC_BILLING')
        ];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, self::URL);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err)
        {
            return [
                'status' => false,
                'errors' => $err,
            ];
        }

        $response = json_decode($response, true);

        if ($response['code'] !== 1)
        {
            $errors = [];

            foreach ($response['errors'] as $attribute => $message)
            {
                $errors[] = implode(',', $message);
            }

            return [
                'status' => false,
                'errors' => implode(' ', $errors),
            ];

        }

        return [
            'status' => true,
            'url' => $response['data']['url'],
            /*'reference' => $cart->id,
            'notificationUrl' => $notificationUrl,
            'errors' => '',
            'response' => $response,
            'data' => $data,*/
        ];
    }

    /**
     * @param Address $address
     * @return string
     */
    private function getAddress(Address $address)
    {
        if (!empty($address->address1))
        {
            return $address->address1;
        }

        return $address->address2;
    }

    private function getPhone(Address $address)
    {
        if (!empty($address->phone_mobile))
        {
            return $address->phone_mobile;
        }

        return $address->phone;
    }

}
