<?php

class PagomediosChargeAjaxModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();

        $this->ajax = true;
    }

    public function displayAjax()
    {
        $billing_company_type = Tools::getValue('billing_company_type');
        $billing_document_type = Tools::getValue('billing_document_type');
        $billing_document = Tools::getValue('billing_document');

        /*$result = $this->module->charge(Tools::getValue("token_id"), Tools::getValue("installments"));
        die(Tools::jsonEncode($result));*/

        $response = $this->module->makeRequest(
            $billing_company_type,
            $billing_document_type,
            $billing_document
        );

        echo json_encode($response);
    }
}
