<?php

require_once(_PS_MODULE_DIR_ . 'pagomedios' . DIRECTORY_SEPARATOR . 'vendor/autoload.php');

class PagoMediosNotificationModuleFrontController extends ModuleFrontController
{
    const URL = 'https://cloud.abitmedia.com/api/payments/create-payment-request';

    public function initContent()
    {
        $this->ajax = false;
        $this->display_column_left = false;
        $this->display_column_right = false;

        try {
            $this->makeRequest();
        } catch (Exception $e) {
            $this->displayMessage($e->getMessage());
        }

        parent::initContent();
    }

    public function makeRequest()
    {
        if (!isset($_POST['status']) || !$_POST['status'] === 'Pagado')
        {
            $template = 'module:pagomedios/views/templates/payment_error.tpl';

            $this->setTemplate(
                $template,
                ['error' => 'Tu pago no fue autorizado, por favor inténtalo nuevamente.']
            );

            return false;
        }

        // authorizationCode
        // status
        // reference
        // clientId

        $cart = new Cart($_POST['reference']);
        $customer = new Customer($cart->id_customer);

        $this->module->validateOrder(
            (int) $cart->id,
            Configuration::get('PAGOMEDIOS_ORDER_STATUS'),
            (float) $cart->getordertotal(true),
            'Pagomedios',
            null,
            array(),
            (int) $cart->id_currency,
            false,
            $customer->secure_key
        );

        Tools::redirect('index.php?controller=order-confirmation&id_cart=' . (int)$cart->id . '&id_module=' . (int)$this->module->id . '&id_order=' . $this->module->currentOrder . '&key=' . $customer->secure_key);

        return true;
    }

    protected function displayMessage($message)
    {
        $this->context->smarty->assign('message', $message);

        if (Tools::version_compare(_PS_VERSION_, '1.7', '>=')) {
            $this->setTemplate('module:safetypay/views/templates/front/ps17/show_message.tpl');
        } else {
            $this->setTemplate('ps16/show_message.tpl');
        }
    }

    private function getCallbackUrl($order_id)
    {
        $callBackUrl = $this->context->link->getModuleLink('safetypay', 'callback');
        $callBackUrl .= (parse_url($callBackUrl, PHP_URL_QUERY) ? '&' : '?') . 'order=' . $order_id;
        return $callBackUrl;
    }

    /**
     * @param Address $address
     * @return string
     */
    private function getAddress(Address $address)
    {
        if (!empty($address->address1))
        {
            return $address->address1;
        }

        return $address->address2;
    }

    private function getPhone(Address $address)
    {
        if (!empty($address->phone_mobile))
        {
            return $address->phone_mobile;
        }

        return $address->phone;
    }
}
