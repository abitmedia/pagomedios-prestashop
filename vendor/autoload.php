<?php
spl_autoload_register(function ($class) {
    if (strpos($class, 'Pagomedios\\') === 0) {
        $className = str_replace('Pagomedios\\', '', $class);
        if (file_exists(_PS_MODULE_DIR_ . 'pagomedios/vendor/src/' . $className . '.php')) {
            include _PS_MODULE_DIR_ . 'pagomedios/vendor/src/' . $className . '.php';
        }
    }

});